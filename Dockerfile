FROM openjdk:17-alpine
COPY build/libs/demo-mc9-0.0.1-SNAPSHOT.jar /app/
WORKDIR /app/
CMD ["java", "-jar", "demo-mc9-0.0.1-SNAPSHOT.jar"]
