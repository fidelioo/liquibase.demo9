package Liquibase.demomc9;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoMc9Application {

    public static void main(String[] args) {
        SpringApplication.run(DemoMc9Application.class, args);
    }

}
