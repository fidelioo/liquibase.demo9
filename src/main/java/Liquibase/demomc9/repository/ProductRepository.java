package Liquibase.demomc9.repository;

import Liquibase.demomc9.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product,Long> {

}
