package Liquibase.demomc9.service;

import Liquibase.demomc9.dto.ProductDto;
import Liquibase.demomc9.model.Product;
import Liquibase.demomc9.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository repository;
    private final ModelMapper mapper;

    @Override
    public ProductDto getProduct(Long id) {
        Product byId=repository.getById(id);
        ProductDto dto= mapper.map(byId,ProductDto.class);
        return dto;
    }

    @Override
    public ProductDto createProduct(ProductDto dto) {
        Product product = mapper.map(dto, Product.class);
        Product save = repository.save(product);
        return mapper.map(save, ProductDto.class);
    }

    @Override
    public void deleteProduct(Long id) {
        repository.deleteById(id);

    }

    @Override
    public ProductDto updateProduct(ProductDto dto) {
        Product product = repository.findById(dto.getId()).orElseThrow((() -> new RuntimeException("Product not found")));
        product.setName(dto.getName());
        product.setPrice(dto.getPrice());
        product.setCreateDate(dto.getCreateDate());
        product.setBrand(dto.getBrand());
        product.setCount(dto.getCount());
        product.setIsActive(dto.getIsActive());
        Product save = repository.save(product);
        return mapper.map(save, ProductDto.class);
    }

}