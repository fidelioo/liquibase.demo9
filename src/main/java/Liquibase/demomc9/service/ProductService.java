package Liquibase.demomc9.service;

import Liquibase.demomc9.dto.ProductDto;


public interface ProductService {

    ProductDto getProduct(Long id);
    ProductDto createProduct(ProductDto dto);
    void deleteProduct(Long id);
    ProductDto updateProduct(ProductDto dto);

}