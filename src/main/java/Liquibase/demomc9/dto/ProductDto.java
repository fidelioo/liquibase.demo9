package Liquibase.demomc9.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class ProductDto {
    private Long  id;
    private String name;
    private Double price;
    private LocalDate createDate;
    private String  brand;
    private Integer  count;
    private Boolean isActive;


}

