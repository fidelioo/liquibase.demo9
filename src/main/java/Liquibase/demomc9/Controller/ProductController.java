package Liquibase.demomc9.Controller;

import Liquibase.demomc9.dto.ProductDto;
import Liquibase.demomc9.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductController {


    private final ProductService service;


    @GetMapping("/{id}")
    public ProductDto getProductById(@PathVariable Long id){
        return service.getProduct(id);
    }


    @PostMapping
    public ProductDto createProduct(@RequestBody ProductDto dto) {
        return service.createProduct(dto);
    }


    @PutMapping
    public ProductDto updateProduct(@RequestBody ProductDto dto) {
        return service.updateProduct(dto);
    }

    @DeleteMapping("/{id}")
    public void deleteProduct(@PathVariable Long id) {
        service.deleteProduct(id);
    }

}

